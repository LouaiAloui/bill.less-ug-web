import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { TodosService } from 'src/app/services/todos.service';

@Component({
  selector: 'app-create-or-update',
  templateUrl: './create-or-update.component.html',
  styleUrls: ['./create-or-update.component.css']
})
export class CreateOrUpdateComponent implements OnInit {

  @Input() todo={title:'',done:false}
  constructor(private todosService: TodosService, private authService: AuthService) { }

  ngOnInit(): void {
    JSON.parse(JSON.stringify(localStorage.getItem('admin')))

  }

  createTodo(){
    this.todosService.newTodo(this.todo).subscribe(data => {
      this.todosService.loadTodos();
    },
    error => {
      console.log(error);
      if (error.status === 401) {
        this.authService.logoutAndRedirect();
      }
    })

  }

}
