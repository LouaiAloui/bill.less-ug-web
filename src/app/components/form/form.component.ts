import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";

import { Todo } from "../../models/todo";
import { TodosService } from "../../services/todos.service";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  
  form = this.fb.group({
    title: ["", Validators.required],
    done: ["", Validators.required],
  });
  submitted = false;


  formData! : Todo
  

  constructor(
    private fb: FormBuilder,
    private todosService: TodosService,
    private authService: AuthService,
  
  ) {
   }

  ngOnInit(): void {
  }

  get titleControl() {
    return this.form.get("title") as FormControl;
  }

  get doneControl() {
    return this.form.get("done") as FormControl;
  }

  createTodo(form: FormGroup){
    this.submitted = true;
    const { value, valid } = form;

    const todo = {
      _id: form.value._id,
      title: form.value.title,
      done: form.value.done
    };

    if(valid){
      if (this.formData !== null) {
        const todoUpdate: Todo = {
          ...todo,
          _id: this.formData._id
        };
       
        this.todosService.updateAdd(todoUpdate, todoUpdate._id).subscribe(
          data => {
            this.reset();
            //this.todosService.changeForm(this.formData);
          },
          error => {
            console.log(error);
            if (error.status === 401) {
              this.authService.logoutAndRedirect();
            }
          }
        );
      }
      if (this.formData === null) {
        this.todosService.newTodo(todo).subscribe(
          data => {
            this.reset();
          },
          error => {
            console.log(error);
            if (error.status === 401) {
              this.authService.logoutAndRedirect();
            }
          }
        );
      }
    }
  }

  private reset() {
    this.form.reset();
    //this.todosService.changeAdd(true);
    this.submitted = false;
  }

}
