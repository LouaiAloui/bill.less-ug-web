import { Component, Inject, Input, OnInit } from '@angular/core';
import { Todo } from "../../models/todo";
import { TodosService } from "../../services/todos.service";
import { AuthService } from "src/app/services/auth.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos: any;
  newTodo: any;
  todoDetail: any = {};
  isUpdate: boolean = false;
  @Input() todo = { title: '', done: false }

  constructor(
    private todosService: TodosService,
    private authService: AuthService,
    private router: Router
  ) {
    this.todos = this.loadTodos();
  };

  ngOnInit(): void {
    this.loadTodos();
    JSON.parse(JSON.stringify(localStorage.getItem('admin')))
    /*this.todosService.currentAdd.subscribe(todo => {
      this.newTodo = todo;
      if (this.newTodo) {
        this.loadTodos();
      }
    })*/
  }

  private loadTodos() {
    this.todosService.loadTodos().subscribe(
      (todos: Todo[]) => {
        this.todos = todos;
      },
      error => {
        console.log(error);
        if (error.status === 401) {
          this.authService.logoutAndRedirect();
        }
      }
    );
  }

  deleteTodo(id: string) {
    this.todosService.deleteTodo(id).subscribe(
      data => {
        this.loadTodos()
      },
      error => {
        console.log(error);
        if (error.status === 401) {
          this.authService.logoutAndRedirect();
        }
      }
    );
  }

  updateTodo(id: string) {
    this.todosService.getTodoById(id).subscribe(data => {
      this.todoDetail = data
      this.isUpdate = true
      this.loadTodos();
    },
    error => {
      console.log(error);
      if (error.status === 401) {
        this.authService.logoutAndRedirect();
      }
    })
  }

  //putTodo(id:string, todo)
  putTodo( todo: Todo, id:string){
    this.todosService.updateAdd(todo,id).subscribe(data => {
      this.loadTodos()
    },
    error => {
      console.log(error);
      if (error.status === 401) {
        this.authService.logoutAndRedirect();
      }
    });
  }

  createTodo() {
    this.todosService.newTodo(this.todo).subscribe(data => {
      this.todosService.loadTodos();
    },
    error => {
      console.log(error);
      if (error.status === 401) {
        this.authService.logoutAndRedirect();
      }
    })
  }
}
