import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  TOKEN_KEY = "admin";

  apiUrl = environment.apiUrl;

  private userToken : any ;


  constructor(private httpClient: HttpClient, private router: Router) {}

  login(username: string, password: string) {
    const endpoint = this.apiUrl + "/auth/login";
    const httpParams = {
      username: username,
      password: password
    };

    return this.httpClient
      .post<{ access_token: string }>(endpoint, httpParams)
      .pipe(
        map(token => {
          this.userToken = token.access_token;
          this.storeToken();
        })
      );
  }



  logoutAndRedirect() {
    this.logout();
    const url = "/login";
    this.router.navigate([url]);
  }

  logout() {
    this.userToken = null;
    this.clearToken();
  }

  getUserToken() {
    console.log(this.userToken)
    return this.userToken;
  }

  isLoggedIn() {
    return typeof this.userToken !== null;
  }

  storeToken() {
    localStorage.setItem(this.TOKEN_KEY, this.getUserToken());
  }

  getStoredToken(){
    localStorage.getItem(this.TOKEN_KEY)
  }

  clearToken() {
    localStorage.removeItem(this.TOKEN_KEY);
  }

  hasStoredToken() {
    const value: any = localStorage.getItem(this.TOKEN_KEY)!;
    return (
      value && value.length > 0
    );
    
  }
}