import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Todo } from "../models/todo";
import { environment } from "src/environments/environment";
import { AuthService } from "./auth.service";
import { SimpleAdd } from "../models/simpleAdd";


@Injectable({ providedIn: 'root' })
export class TodosService {

  add! : Todo
  private addSource = new BehaviorSubject(false);
  currentAdd = this.addSource.asObservable();
  private formData = new BehaviorSubject(this.add);
  currentForm = this.formData.asObservable();


  constructor(
    private httpClient: HttpClient,
    private authService: AuthService,
  ) { }


  private apiUrl = environment.apiUrl;
   
  private httpOptions = {
    headers: new HttpHeaders()
      .set("Authorization", `Bearer ${this.authService.getUserToken()}`)
  };

  loadTodos(): Observable<any> {
    const endpoint = this.apiUrl + "/todos";

    return this.httpClient.get<Todo[]>(endpoint, this.httpOptions);
  }

  newTodo(todo: SimpleAdd) {
    const endpoint = this.apiUrl + "/todos";
    return this.httpClient.post(endpoint, todo, this.httpOptions);
  }

  saveTodo(todos:{}) {
    const endpoint = this.apiUrl + `/todos`;

    return this.httpClient.put(endpoint, todos, this.httpOptions);
  }

  updateAdd(add: Todo, id: string) {
    const endpoint = this.apiUrl + `/todos/${id}`;

    return this.httpClient.put(endpoint, add, this.httpOptions);
  }

  deleteTodo(id: string) {
    const endpoint = this.apiUrl + `/todos/${id}`;

    return this.httpClient.delete<Todo[]>(endpoint, this.httpOptions);
  }

  getTodoById(id:string){
    const endpoint = this.apiUrl + `/todos/${id}`;

    return this.httpClient.get<Todo>(endpoint, this.httpOptions);
  }

  getAccessToken() {
    return sessionStorage.getItem(this.authService.TOKEN_KEY);
  }
}
