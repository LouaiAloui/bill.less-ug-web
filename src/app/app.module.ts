import { NgModule , CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AdminComponent } from './container/admin/admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './routing/routing.module';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guards';
import {RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { TodosComponent } from './components/todos/todos.component';
import { FormComponent } from './components/form/form.component';
import { TodosService } from './services/todos.service';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CreateOrUpdateComponent } from './components/create-or-update/create-or-update.component';
import { HeaderComponent } from './components/header/header.component';

import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    TodosComponent,
    FormComponent,
    NotFoundComponent,
    CreateOrUpdateComponent,
    HeaderComponent,
    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    CommonModule,
    FormsModule,
    NzMenuModule,
    NzGridModule,
    NzButtonModule,
    NzTableModule,
    NzFormModule,
    NzInputModule,
    NzCardModule,
    NzCheckboxModule
  ],
  providers: [TodosService, AuthService, AuthGuard, ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
