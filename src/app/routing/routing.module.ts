import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminComponent } from "../container/admin/admin.component";
import { LoginComponent } from "../components/login/login.component";
import { AuthGuard } from "../guards/auth.guards";
import { NotFoundComponent } from "../components/not-found/not-found.component";
import { TodosComponent } from "../components/todos/todos.component";
import { FormComponent } from "../components/form/form.component";
import { CreateOrUpdateComponent } from "../components/create-or-update/create-or-update.component";

const routes: Routes = [
  { path: "admin", component: AdminComponent, canActivate: [AuthGuard] },
  { path: "", redirectTo: "admin", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "404", component: NotFoundComponent },
  { path: "**", redirectTo: "404" },
  { path: "todos", component: TodosComponent, outlet: "<app-todos></app-todos>"},
  { path: "", component: FormComponent, outlet: "<app-form></app-form>"},
  { path: "create", component: CreateOrUpdateComponent, outlet: "<app-create-or-update></app-create-or-update>"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}